# eCommerce-ui

Sample eCommerce application with microservice architecture.

## Tech used

* Spring Boot
* MySql
* Angular
* JWT

## ToDo

| Status | Description |
| --- | --- |
| ☑️| Create UI components for User Registration and Sign in |
| ☑️| Create UI components for Create-Orders, Order-List, Delete Orders |