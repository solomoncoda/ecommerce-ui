export class User {
    userId: number;
    userName: string;
    password: string;
    postalAddress: string;
    contactNumber: number;
    email: string;
    token?: string;
}
