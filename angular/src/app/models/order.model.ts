export class Order {
    orderId: number;
    userName: string;
    productName: string;
    shippingAddress: string;
}
