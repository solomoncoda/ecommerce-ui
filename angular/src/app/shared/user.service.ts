import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private formBuilder: FormBuilder, private http: HttpClient) { }
  readonly BaseURI = 'http://localhost:8200';

  formModel = this.formBuilder.group({
    userName: ['', Validators.required],
    email: ['', Validators.email],
    Passwords: this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(4)]],
      ConfirmPassword: ['', Validators.required]
    }, { validator: this.comparePasswords }),
    contactNumber: ['', Validators.required]
  });

  comparePasswords(fb: FormGroup) {
    const confirmPswrdCtrl = fb.get('ConfirmPassword');
    // passwordMismatch
    // confirmPswrdCtrl.errors={passwordMismatch:true}
    if (confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if (fb.get('Password').value !== confirmPswrdCtrl.value) {
        confirmPswrdCtrl.setErrors({ passwordMismatch: true });
      } else {
        confirmPswrdCtrl.setErrors(null);
      }
    }
  }

  register() {
    const body = {
      userName: this.formModel.value.userName,
      email: this.formModel.value.email,
      password: this.formModel.value.Passwords.password,
      postalAddress: this.formModel.value.postalAddress,
      contactNumber: this.formModel.value.contactNumber
    };
    return this.http.post(this.BaseURI + '/user', body);
  }

  login(formData) {
    return this.http.post(this.BaseURI + '/user/authenticate', formData);
  }

  getUserProfile() {
    return this.http.get(this.BaseURI + '/user');
  }
}
