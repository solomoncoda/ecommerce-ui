import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Order } from '../models/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  public formData: Order;
  public orderList: Order[];
  readonly rootUrl = 'http://localhost:8201';

  constructor(private http: HttpClient) { }

  postOrder(order: Order) {
    return this.http.post(this.rootUrl + '/order/create', this.formData);
  }

  getOrdersRefresh() {
    this.getOrders().subscribe(
      res => {
        this.orderList = res as Order[];
      },
      err => {
        console.log(err);
      }, 
    );
  }
  getOrders() {
    return this.http.get(this.rootUrl + '/order/orders');
  }
  putOrder(order: Order) {
    return this.http.put(this.rootUrl + '/order', this.formData);
  }

  deleteOrder(order: Order) {
    return this.http.delete(this.rootUrl + '/order' + '/' + order.orderId);
  }
}
