import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/shared/order.service';
import { ToastrService } from 'ngx-toastr';
import { Order } from 'src/app/models/order.model';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {

  constructor(private service: OrderService, private toastr: ToastrService) { }

  ngOnInit() {
    this.service.getOrdersRefresh();
  }

  populateOrderForm(order: Order) {
    this.service.formData = Object.assign({}, order);
  }

  onDelete(order: Order) {
    this.service.deleteOrder(order).subscribe(res => {
      this.toastr.success('User Deleted.', 'eCommerce');
      this.service.getOrdersRefresh();
    });
  }

}
