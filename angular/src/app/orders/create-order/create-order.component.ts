import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/shared/order.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Order } from 'src/app/models/order.model';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit {

  constructor(private service: OrderService, private toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm){
    if(form != null){
      form.resetForm();
    }

    this.service.formData = new Order();
  }

  onSubmit(form: NgForm){
    if(form.value.orderId == null){
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }

  }

  insertRecord(form: NgForm) {
    this.service.postOrder(form.value).subscribe(res => {
      this.toastr.success('New order created.', 'eCommerce');
      this.service.getOrdersRefresh();
      this.resetForm(form);
    });

  }

  updateRecord(form: NgForm) {
    this.service.putOrder(form.value).subscribe(res => {
      this.toastr.success('Order Updated.', 'eCommerce');
      this.resetForm(form);
      this.service.getOrdersRefresh();
    });
  }

}
